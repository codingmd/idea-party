import os
import pkgutil
import sys


def get_package_and_instance_paths(name):
    loader = pkgutil.get_loader(name)
    filename = os.path.abspath(loader.get_filename(name))
    package_path = os.path.dirname(filename)
    root_path = os.path.dirname(package_path)
    site_parent, site_folder = os.path.split(root_path)
    py_prefix = os.path.abspath(sys.prefix)

    if root_path.startswith(py_prefix):
        base_dir = py_prefix

    elif site_folder.lower() == 'site-packages':
        parent, folder = os.path.split(site_parent)

        if folder.lower() == 'lib':
            base_dir = parent

        elif os.path.basename(parent).lower() == 'lib':
            base_dir = os.path.dirname(parent)

        else:
            base_dir = site_parent

    else:
        return package_path, os.path.join(root_path, 'instance')

    return package_path, os.path.join(base_dir, 'var', name + '-instance')


def settings_from_file(filename, namespace):
    try:
        with open(filename) as f:
            source = f.read()
    except IOError as e:
        e.strerror = 'Unable to load configuration file ({})'.format(e.strerror)
        raise

    settings = {}
    exec(compile(source, filename, 'exec'), settings)

    for key, value in settings.items():
        if key.isupper():
            namespace[key] = value


PACKAGE_PATH, INSTANCE_PATH = get_package_and_instance_paths('idea_party')

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.staticfiles',
    'bootstrap3',
    'idea_party.auth.apps.AuthConfig',
    'idea_party.ideas',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'idea_party.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(PACKAGE_PATH, 'templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(INSTANCE_PATH, 'db.sqlite3'),
    }
}

LOGIN_REDIRECT_URL = 'index'
LOGOUT_REDIRECT_URL = 'index'

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

STATICFILES_DIRS = [
    os.path.join(PACKAGE_PATH, 'static'),
]
STATIC_ROOT = os.path.join(INSTANCE_PATH, 'static')
STATIC_URL = '/static/'
MEDIA_ROOT = os.path.join(INSTANCE_PATH, 'media')
MEDIA_URL = '/media/'

SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_BROWSER_XSS_FILTER = True
CSRF_COOKIE_HTTPONLY = True
X_FRAME_OPTIONS = 'DENY'
USE_ETAGS = True
SESSION_EXPIRE_AT_BROWSER_CLOSE = True

settings_from_file(os.path.join(INSTANCE_PATH, 'settings.py'), globals())
