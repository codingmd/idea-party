#!/usr/bin/env python
import os

from setuptools import find_packages, setup

setup(
    name='idea-party',
    version='1.0',
    packages=find_packages(os.path.dirname(__file__)),
    install_requires=[
        'django',
        'django-bootstrap3',
    ],
    entry_points={
        'console_scripts': [
            'idea-party=idea_party.manage:execute_from_command_line',
        ],
    },
)
